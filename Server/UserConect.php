<?php 
	require_once('./DBconect.php');
	/**
	* User Authentication
	*/
	class UserAuthen extends CD{
		private $exit = array();

		function __construct(){
			if($this->inSession($_POST['key'])){
				if(isset($_POST['logout'])){
					$this->logout($_POST['key']);
				}
				else{
					$this->login($_POST['key']);
				}
			}
			elseif(isset($_POST['user']) && isset($_POST['pass'])){
				$this->valLogin($_POST['user'], $_POST['pass']);
			}
			echo json_encode($this->exit, JSON_UNESCAPED_UNICODE);
		}

		private function inSession($key){
			return (in_array("sess_".$key,scandir(session_save_path())))? true : false;
		}

		private function logout($key){
			session_id($key);
			session_start();
			session_unset();
			session_destroy();
			$_SESSION = array();
			$this->exit['user'] = "Desconectado";
		}

		private function login($key){
			session_id($key);
			session_start();
			$this->exit['user'] = $_SESSION['user_nick'];
			$this->exit['id'] = $_SESSION['user_id'];
		}

		private function valLogin($user, $pass){
			$consult = $this->conectionUser($user, $pass);
			if($consult){
				$key = uniqid();
				session_id($key);
				session_start();
				$_SESSION['user_nick'] = $consult['user'];
				$_SESSION['user_id'] = $consult['id'];
				$this->exit['key'] = $key;
			}else $this->exit['erro'] = 'Senha Inválida';
		}
	}