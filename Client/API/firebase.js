  var telefones = [{"nome":"Central de monitoramento","telefone":"08006435508","img":"tor.jpg"},{"nome":"Serviço Social","telefone":"8532523411","img":"sc.jpg"},{"nome":"CISPE","telefone":"8531017720","img":"cispe.jpg"},{"nome":"CAP","telefone":"8531017723","img":"cap.jpg"}];
  var videos = [{nome: "Tornozeleira", url: "GvtCLWaehRg"}];
  if(typeof firebase != "undefined")
    (function(){
  	var config = {
    apiKey: "AIzaSyChUSx3Qe6rKtSmfw0cJNn-68zUu6qJN7A",
    authDomain: "estudos-faculdade.firebaseapp.com",
    databaseURL: "https://estudos-faculdade.firebaseio.com",
    projectId: "estudos-faculdade",
    storageBucket: "estudos-faculdade.appspot.com",
    messagingSenderId: "293564310664"
  };
  firebase.initializeApp(config);

  //Codigo Fonte
  const DBtel = firebase.database().ref().child('AjudaAPI/Telefones');
  DBtel.on('value', 
    snap => {
      var contatos = "";
      localStorage.clear();
      snap.val().map(function(e){
        localStorage.setItem(localStorage.length,JSON.stringify(e));
        contatos+='<li class="collection-item avatar"><img src="img/'+e['img']+'" alt="" class="circle"><span class="title">'+e['nome']+'</span><p>Contato <br></p><a href="tel:'+e['telefone']+'" class="secondary-content"><i class="material-icons">call</i></a></li>';
      });
      $('.collapsible-body')[0].innerHTML = contatos;
    }
  );
  const DBvid = firebase.database().ref().child('AjudaAPI/Videos');
  DBvid.on('value', 
  	snap => {
      for(i=0;i<localStorage.length;i++){
        e = JSON.parse(localStorage.getItem(i));
        if(typeof e['url'] != "undefined"){
          localStorage.removeItem(i);
        }
      }
  		var videos = "";
  		snap.val().map(function(e){
        var p = localStorage.length;
        var urrl = "'"+e['url']+"'";
  			localStorage.setItem(p,JSON.stringify(e));
  			videos+='<li><div class="collapsible-header" onclick="v('+urrl+','+p+')"><i class="material-icons">filter_drama</i>'+e['nome']+'</div><div class="collapsible-body"><div class="video-container"><iframe id="video'+p+'" frameborder="0" allowfullscreen></iframe></div></div></li>';
  		});
      $('.collapsible ul')[0].innerHTML = videos;
  	}
  );
}());